# README #

In this repository is contained an API Rest built for gateway management and device association, 
exposing a series of endpoints where you can perform these operations, to view the documentation of the project 
you can enter the following url  http://localhost:8089/api/swagger-ui.html once you run the project locally

### Summary of set up 

It must be started by installing jdk 11, along with maven and docker to proceed 
with the configuration, for this we are going to raise an image that has the database 
to which it will be connected through the configuration registered in the application.properties of the project

### Dependencies
* `Docker`
* `Jdk 11`
* `Maven`

### Configuration
Once the docker is installed, the following commands must be executed, which will result in the configuration of the database locally

```
docker pull mysql
```
```
docker run -p 3306:3306 --name some-mysql -e MYSQL_ROOT_PASSWORD=chattigo -d mysql:8.0
```

### Database configuration
To change the database configuration you can go to the application.properties file, 
inside you will find the connection properties that you can manipulate in case 
you do not want to connect to the docker database

inside resource you will also find the db.migration directory where two .sql files are stored, ```V1__Initial_schema.sql```
which contains the ddl for the creation of the tables using flyway. ```R__1_data.sql``` where the scripts to populate the tables 
with test data are stored.

### How to run tests
to run the tests you must use the command ```mvn clean test```

### Build jar instructions
to run the build task you mush use the command ```mvn clean install -U```


