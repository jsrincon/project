package com.example.chattigo.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import com.example.chattigo.model.Gateway;
import com.example.chattigo.model.dto.GatewayDto;
import com.example.chattigo.model.dto.PeripheralDeviceDto;
import com.example.chattigo.repository.GatewayRepository;
import com.example.chattigo.repository.specification.GatewaySerialNumberSpecification;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class GatewayServiceImplTest {

    @Mock
    private GatewayRepository gatewayRepository;

    @InjectMocks
    private GatewayServiceImpl gatewayService;

    @ParameterizedTest(name = "{0}")
    @MethodSource("com.example.chattigo.arguments.ArgumentCreate#arguments")
    void create(String nameTest,GatewayDto request, Gateway mockGateway, Gateway response, Exception exception) {
        try {

            Optional<Gateway> optionalMock = null != mockGateway ? Optional.of(mockGateway) : Optional.empty();

            given(gatewayRepository.findOne(any(GatewaySerialNumberSpecification.class)))
                    .willReturn(optionalMock);

            given(gatewayRepository.save(any())).willReturn(response);

            GatewayDto gatewayDto = gatewayService.create(request);

            assertThat(gatewayDto).isEqualTo(request);


        } catch (Exception e) {

            assertThat(e.getClass()).isEqualTo(exception.getClass());
            assertThat(e.getMessage()).isEqualTo(exception.getMessage());
        }
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("com.example.chattigo.arguments.ArgumentAddDevice#arguments")
    void addDevice(String nameTest, String serialNumber, PeripheralDeviceDto request, Gateway mockGateway, Gateway response, Exception exception) {
        try {

            Optional<Gateway> optionalMock = null != mockGateway ? Optional.of(mockGateway) : Optional.empty();

            given(gatewayRepository.findOne(any(GatewaySerialNumberSpecification.class)))
                    .willReturn(optionalMock);

            given(gatewayRepository.save(any())).willReturn(response);

            GatewayDto gatewayDto = gatewayService.addDevice(request, serialNumber);

            assertThat(gatewayDto).isNotNull();


        } catch (Exception e) {

            assertThat(e.getClass()).isEqualTo(exception.getClass());
            assertThat(e.getMessage()).isEqualTo(exception.getMessage());
        }
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("com.example.chattigo.arguments.ArgumentRemoveDevice#arguments")
    void removeDevice(String nameTest, String serialNumber, Integer uid, Gateway mockGateway, Gateway response, Exception exception) {
        try {

            Optional<Gateway> optionalMock = null != mockGateway ? Optional.of(mockGateway) : Optional.empty();

            given(gatewayRepository.findOne(any(GatewaySerialNumberSpecification.class)))
                    .willReturn(optionalMock);

            given(gatewayRepository.save(any())).willReturn(response);

            GatewayDto gatewayDto = gatewayService.removeDevice(serialNumber, uid);

            assertThat(gatewayDto).isNotNull();


        } catch (Exception e) {

            assertThat(e.getClass()).isEqualTo(exception.getClass());
            assertThat(e.getMessage()).isEqualTo(exception.getMessage());
        }
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("com.example.chattigo.arguments.ArgumentFindAll#arguments")
    void getAll(String nameTest, List<Gateway> mockGateways, Integer expect) {

        given(gatewayRepository.findAll()).willReturn(mockGateways);

        List<GatewayDto> gateways = gatewayService.getAll();

        assertThat(gateways).isNotNull();

        assertThat(gateways.size()).isEqualTo(expect);

    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("com.example.chattigo.arguments.ArgumentFindBySerialNumber#arguments")
    void getBySerialNumber(String nameTest, String serialNumber, Gateway mockGateway, Exception exception) {
        try {

            Optional<Gateway> optionalMock = null != mockGateway ? Optional.of(mockGateway) : Optional.empty();

            given(gatewayRepository.findOne(any(GatewaySerialNumberSpecification.class)))
                    .willReturn(optionalMock);

            GatewayDto gatewayDto = gatewayService.getBySerialNumber(serialNumber);

            Assertions.assertNotNull(gatewayDto);


        } catch (Exception e) {

            assertThat(e.getClass()).isEqualTo(exception.getClass());
            assertThat(e.getMessage()).isEqualTo(exception.getMessage());
        }
    }
}