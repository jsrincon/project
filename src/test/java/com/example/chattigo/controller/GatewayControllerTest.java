package com.example.chattigo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.chattigo.constants.CodeError;
import com.example.chattigo.constants.MessageError;
import com.example.chattigo.exception.DeviceNotFoundException;
import com.example.chattigo.exception.DevicesExceedsAllowedSizeException;
import com.example.chattigo.exception.GatewayNotFoundException;
import com.example.chattigo.exception.NoAssociatedDevicesException;
import com.example.chattigo.exception.SerialNumberAlreadyExistsGatewayException;
import com.example.chattigo.factory.GatewayFactory;
import com.example.chattigo.model.dto.GatewayDto;
import com.example.chattigo.model.dto.PeripheralDeviceDto;
import com.example.chattigo.service.IGatewayService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.util.Collections;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.annotation.Validated;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Validated
class GatewayControllerTest {

    public static final String GATEWAY_SERIAL_SEARCH = "/v0/gateways/12345asd";
    public static final String GATEWAY_ADD_DEVICES = "/v0/gateways/12345asd/devices";
    public static final String GATEWAY_REMOVE_DEVICE = "/v0/gateways/12345asd/devices/1";
    public static final String URL_BASE_GATEWAY = "/v0/gateways";

    private String jsonCreateGatewayOk;
    private String jsonAddDevicesOk;
    private String jsonCreateGatewayInvalidIp;
    private String jsonCreateGatewayInvalidIpNumber;
    private String jsonCreateGatewayStatusInvalid;

    @Autowired
    private MockMvc mvc;
    @MockBean
    private IGatewayService gatewayService;
    private GatewayDto gateway;


    @BeforeEach
    @SneakyThrows
    void setUp() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        gateway = new GatewayFactory().buildDto();

        jsonCreateGatewayOk = objectMapper.writeValueAsString(
                objectMapper.readValue(ClassLoader.getSystemClassLoader().getResourceAsStream("json/create_gateway_ok.json"),
                        GatewayDto.class));

        jsonAddDevicesOk = objectMapper.writeValueAsString(
                objectMapper.readValue(ClassLoader.getSystemClassLoader().getResourceAsStream("json/create_devices_ok.json"),
                        PeripheralDeviceDto.class));


        jsonCreateGatewayStatusInvalid = objectMapper.writeValueAsString(
                objectMapper.readValue(ClassLoader.getSystemClassLoader().getResourceAsStream("json/create_gateway_bad_status.json"),
                        GatewayDto.class));

        jsonCreateGatewayInvalidIp = objectMapper.writeValueAsString(new GatewayFactory()
                .setIpv4Address("invalidIp")
                .setPeripheralDevices(Collections.emptyList())
                .buildDto());

        jsonCreateGatewayInvalidIpNumber = objectMapper.writeValueAsString(new GatewayFactory()
                .setIpv4Address("192.168.5.500")
                .setPeripheralDevices(Collections.emptyList())
                .buildDto());
    }

    @Test
    @SneakyThrows
    void giveJsonOkWhenCreateThenCreated() {

        given(gatewayService.create(any())).willReturn(new GatewayFactory().buildDto());

        mvc.perform(post(URL_BASE_GATEWAY)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonCreateGatewayOk))
                .andExpect(status().isCreated());
    }

    @Test
    @SneakyThrows
    void giveAlreadyExistGatewayWhenCreateThenCreated() {

        given(gatewayService.create(any())).willThrow(SerialNumberAlreadyExistsGatewayException.class);

        mvc.perform(post(URL_BASE_GATEWAY)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCreateGatewayOk))
                .andExpect(status().isBadRequest());
    }

    @Test
    @SneakyThrows
    void giveJsonMalformedWhenCreateThenBadRequest() {

        given(gatewayService.create(any())).willReturn(gateway);

        String res = mvc.perform(post(URL_BASE_GATEWAY)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCreateGatewayInvalidIp))
                .andExpect(status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(res).contains(MessageError.INVALID_VALUES);
    }

    @Test
    @SneakyThrows
    void giveNumberIpErrorWhenCreateThenBadRequest() {

        given(gatewayService.create(any())).willReturn(gateway);

        String res = mvc.perform(post(URL_BASE_GATEWAY)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCreateGatewayInvalidIpNumber))
                .andExpect(status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(res).contains(MessageError.INVALID_VALUES);
    }

    @Test
    @SneakyThrows
    void giveJsonStatusInvalidWhenCreateThenBadRequest() {

        given(gatewayService.create(any())).willReturn(new GatewayFactory().buildDto());

        String res = mvc.perform(post(URL_BASE_GATEWAY)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCreateGatewayStatusInvalid))
                .andExpect(status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(res).contains(MessageError.INVALID_VALUES);
    }

    @Test
    @SneakyThrows
    void getAllThenReturnList() {
        given(gatewayService.getAll()).willReturn(List.of(gateway));

        mvc.perform(get(URL_BASE_GATEWAY)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void getAllThenReturnEmptyList() {
        given(gatewayService.getAll()).willReturn(Collections.emptyList());

        mvc.perform(get(URL_BASE_GATEWAY)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void getBySerialNumberThenReturn() {
        given(gatewayService.getBySerialNumber(anyString())).willReturn(gateway);

        mvc.perform(get(GATEWAY_SERIAL_SEARCH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonAddDevicesOk))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void getBySerialNumberThenError() {
        given(gatewayService.getBySerialNumber(anyString())).willThrow(GatewayNotFoundException.class);

        mvc.perform(get(GATEWAY_SERIAL_SEARCH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonAddDevicesOk))
                .andExpect(status().isNotFound());
    }

    @Test
    @SneakyThrows
    void addDeviceThenOk() {
        given(gatewayService.addDevice(any(),any())).willReturn(gateway);

        mvc.perform(patch(GATEWAY_ADD_DEVICES)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonAddDevicesOk))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void addDeviceThenError() {
        given(gatewayService.addDevice(any(),any())).willThrow(DevicesExceedsAllowedSizeException.class);

        mvc.perform(patch(GATEWAY_ADD_DEVICES)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonAddDevicesOk))
                .andExpect(status().isBadRequest());
    }

    @Test
    @SneakyThrows
    void removeDeviceThenOk() {
        given(gatewayService.removeDevice(any(),any())).willReturn(gateway);

       mvc.perform(delete(GATEWAY_REMOVE_DEVICE)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    @SneakyThrows
    void givenDeviceNotFoundWhenRemoveDeviceThenError() {
        given(gatewayService.removeDevice(any(),any())).willThrow(DeviceNotFoundException.class);

        String res = mvc.perform(delete(GATEWAY_REMOVE_DEVICE)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(res).contains(CodeError.NOT_FOUND_DEVICE_ERROR_CODE);
    }

    @Test
    @SneakyThrows
    void givenEmptyListWhenRemoveDeviceThenError() {
        given(gatewayService.removeDevice(any(),any())).willThrow(NoAssociatedDevicesException.class);

        String res = mvc.perform(delete(GATEWAY_REMOVE_DEVICE)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(res).contains(CodeError.ASSOCIATED_DEVICE_ERROR_CODE);
    }
}