package com.example.chattigo.factory;

import com.example.chattigo.model.Gateway;
import com.example.chattigo.model.PeripheralDevice;
import com.example.chattigo.model.dto.GatewayDto;
import java.util.ArrayList;
import java.util.List;

public class GatewayFactory {

    private Integer id = 1;

    private String serialNumber = "A123456789";

    private String humanReadableName = "GATEWAY Nº 1";

    private String ipv4Address = "192.168.0.1";

    private List<PeripheralDevice> peripheralDevices = new ArrayList<>(List.of(new PeripheralDeviceFactory().build()));

    public Gateway build(){
        return  Gateway.builder()
                .id(id)
                .serialNumber(serialNumber)
                .humanReadableName(humanReadableName)
                .ipv4Address(ipv4Address)
                .peripheralDevices(peripheralDevices)
                .build();
    }

    public GatewayDto buildDto(){
        return GatewayDto.entityToDto(Gateway.builder()
                .id(id)
                .serialNumber(serialNumber)
                .humanReadableName(humanReadableName)
                .ipv4Address(ipv4Address)
                .peripheralDevices(peripheralDevices)
                .build());
    }

    public GatewayFactory setId(Integer id) {
        this.id = id;
        return this;
    }

    public GatewayFactory setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
        return this;
    }

    public GatewayFactory setHumanReadableName(String humanReadableName) {
        this.humanReadableName = humanReadableName;
        return this;
    }

    public GatewayFactory setIpv4Address(String ipv4Address) {
        this.ipv4Address = ipv4Address;
        return this;
    }

    public GatewayFactory setPeripheralDevices(
            List<PeripheralDevice> peripheralDevices) {
        this.peripheralDevices = peripheralDevices;
        return this;
    }
}

