package com.example.chattigo.factory;

import com.example.chattigo.model.Gateway;
import com.example.chattigo.model.PeripheralDevice;
import com.example.chattigo.model.dto.PeripheralDeviceDto;
import com.example.chattigo.model.enums.StatusDevices;
import java.time.LocalDate;

public class PeripheralDeviceFactory {

    private Integer uid = 1;

    private String vendor = "Samsung";

    private LocalDate dateCreated = LocalDate.now();

    private StatusDevices status = StatusDevices.ONLINE;

    private Gateway gateway ;

    public PeripheralDevice build() {
        return PeripheralDevice.builder()
                .id(uid)
                .vendor(vendor)
                .dateCreated(dateCreated)
                .status(status)
                .gateway(gateway)
                .build();
    }

    public PeripheralDeviceDto buildDto(){
        return PeripheralDeviceDto.entityToDto(PeripheralDevice.builder()
                .id(uid)
                .vendor(vendor)
                .dateCreated(dateCreated)
                .status(status)
                .gateway(gateway)
                .build());
    }

    public PeripheralDeviceFactory setUid(Integer uid) {
        this.uid = uid;
        return this;
    }

    public PeripheralDeviceFactory setVendor(String vendor) {
        this.vendor = vendor;
        return this;
    }

    public PeripheralDeviceFactory setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public PeripheralDeviceFactory setStatus(StatusDevices status) {
        this.status = status;
        return this;
    }

    public PeripheralDeviceFactory setGateway(Gateway gateway) {
        this.gateway = gateway;
        return this;
    }

}
