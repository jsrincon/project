package com.example.chattigo.arguments;
import com.example.chattigo.exception.DevicesExceedsAllowedSizeException;
import com.example.chattigo.exception.GatewayNotFoundException;
import com.example.chattigo.exception.SerialNumberAlreadyExistsGatewayException;
import com.example.chattigo.factory.GatewayFactory;
import com.example.chattigo.factory.PeripheralDeviceFactory;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class ArgumentAddDevice {

    private static Stream<Arguments> arguments(){
        return Stream.of(
                Arguments.of(
                        "add device in gateway ok",
                        "A123456789",
                        new PeripheralDeviceFactory().buildDto(),
                        new GatewayFactory().build(),
                        new GatewayFactory().build(),
                        null
                ),
                Arguments.of(
                        "gateway not found",
                        "A123456789",
                        new PeripheralDeviceFactory().buildDto(),
                        null,
                        new GatewayFactory().build(),
                        new GatewayNotFoundException()
                ),
                Arguments.of(
                        "Size exceeds of peripheral devices exception",
                        "A123456789",
                        new PeripheralDeviceFactory().buildDto(),
                        new GatewayFactory()
                                .setPeripheralDevices(List.of(
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build()
                                )).build(),
                        new GatewayFactory().build(),
                        new DevicesExceedsAllowedSizeException()
                )
        );
    }

}
