package com.example.chattigo.arguments;
import com.example.chattigo.exception.DevicesExceedsAllowedSizeException;
import com.example.chattigo.exception.SerialNumberAlreadyExistsGatewayException;
import com.example.chattigo.factory.GatewayFactory;
import com.example.chattigo.factory.PeripheralDeviceFactory;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class ArgumentCreate {

    private static Stream<Arguments> arguments(){
        return Stream.of(
                Arguments.of(
                        "Create gateway ok",
                        new GatewayFactory().buildDto(),
                        null,
                        new GatewayFactory().build(),
                        null
                ),
                Arguments.of(
                        "Create gateway without devices ok",
                        new GatewayFactory().setPeripheralDevices(null).buildDto(),
                        null,
                        new GatewayFactory().setPeripheralDevices(null).build(),
                        null
                ),
                Arguments.of(
                        "Serial number already exist exception",
                        new GatewayFactory().buildDto(),
                        new GatewayFactory().build(),
                        new GatewayFactory().build(),
                        new SerialNumberAlreadyExistsGatewayException()
                ),
                Arguments.of(
                        "Size exceeds of peripheral devices exception",
                        new GatewayFactory()
                                .setPeripheralDevices(List.of(
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build(),
                                        new PeripheralDeviceFactory().build()
                                ))
                                .buildDto(),
                        null,
                        new GatewayFactory().build(),
                        new DevicesExceedsAllowedSizeException()
                )
        );
    }

}
