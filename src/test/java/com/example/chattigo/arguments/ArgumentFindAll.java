package com.example.chattigo.arguments;
import com.example.chattigo.factory.GatewayFactory;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class ArgumentFindAll {

    private static Stream<Arguments> arguments(){
        return Stream.of(
                Arguments.of(
                        "find all gateway  ok",
                        List.of(new GatewayFactory().build(), new GatewayFactory().build(), new GatewayFactory().build()),
                        3
                ),
                Arguments.of(
                        "find all empty ok",
                        Collections.emptyList(),
                        0
                )
        );
    }

}
