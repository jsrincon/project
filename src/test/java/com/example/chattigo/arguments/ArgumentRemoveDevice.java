package com.example.chattigo.arguments;
import com.example.chattigo.exception.DeviceNotFoundException;
import com.example.chattigo.exception.DevicesExceedsAllowedSizeException;
import com.example.chattigo.exception.GatewayNotFoundException;
import com.example.chattigo.exception.NoAssociatedDevicesException;
import com.example.chattigo.factory.GatewayFactory;
import com.example.chattigo.factory.PeripheralDeviceFactory;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class ArgumentRemoveDevice {

    private static Stream<Arguments> arguments(){
        return Stream.of(
                Arguments.of(
                        "remove device in gateway ok",
                        "A123456789",
                        1,
                        new GatewayFactory().build(),
                        new GatewayFactory().build(),
                        null
                ),
                Arguments.of(
                        "gateway not found",
                        "A123456789",
                        1,
                        null,
                        new GatewayFactory().build(),
                        new GatewayNotFoundException()
                ),
                Arguments.of(
                        "empty list of peripheral devices exception",
                        "A123456789",
                        1,
                        new GatewayFactory().setPeripheralDevices(Collections.emptyList()).build(),
                        new GatewayFactory().build(),
                        new NoAssociatedDevicesException()
                ),
                Arguments.of(
                        "empty list of peripheral devices exception",
                        "A123456789",
                        12134,
                        new GatewayFactory().build(),
                        new GatewayFactory().build(),
                        new DeviceNotFoundException()
                )
        );
    }

}
