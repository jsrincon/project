package com.example.chattigo.arguments;
import com.example.chattigo.exception.GatewayNotFoundException;
import com.example.chattigo.exception.NoAssociatedDevicesException;
import com.example.chattigo.factory.GatewayFactory;
import java.util.Collections;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class ArgumentFindBySerialNumber {

    private static Stream<Arguments> arguments(){
        return Stream.of(
                Arguments.of(
                        "find gateway  ok",
                        "A123456789",
                        new GatewayFactory().build(),
                        null
                ),
                Arguments.of(
                        "gateway not found",
                        "A123456789",
                        null,
                        new GatewayNotFoundException()
                )
        );
    }

}
