package com.example.chattigo.constants;

public class CodeError {

    public static final String INVALID_PARAMETERS_ERROR_CODE = "INVALID_PARAMETERS";
    public static final String GATEWAY_NOT_FOUND_ERROR_CODE = "GATEWAY_NOT_FOUND";
    public static final String SERIAL_ALREADY_EXIST_GATEWAY_ERROR_CODE = "SERIAL_ALREADY_EXIST_GATEWAY_ERROR_CODE";
    public static final String ASSOCIATED_DEVICE_ERROR_CODE = "ASSOCIATED_DEVICE_ERROR_CODE";
    public static final String NOT_FOUND_DEVICE_ERROR_CODE = "NOT_FOUND_DEVICE_ERROR_CODE";

    private CodeError() {
    }
}
