package com.example.chattigo.constants;

public class MessageError {

    public static final String EXCEEDS_SIZE_DEVICE = "Devices exceed allowed size which is 10 per gateway.";
    public static final String NOT_FOUND_GATEWAY = "The gateway was not found, check the id entered.";
    public static final String SERIAL_NUMBER_ALREADY_GATEWAY = "The serial number is already registered in a gateway";
    public static final String NO_ASSOCIATED_DEVICE = "no devices associated with the gateway";
    public static final String NO_FOUND_DEVICE = "the device id is not registered in the gateway";
    public static final String INVALID_VALUES = "Invalid values on parameters";

    private MessageError() {
    }
}
