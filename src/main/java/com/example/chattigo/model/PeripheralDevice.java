package com.example.chattigo.model;

import com.example.chattigo.model.enums.StatusDevices;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "peripheral_device")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PeripheralDevice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UID", nullable = false)
    private Integer id;

    @Column(name = "vendor", nullable = false)
    private String vendor;

    @Column(name = "date_created", nullable = false)
    private LocalDate dateCreated;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusDevices status;

    @ManyToOne(optional = false)
    @JoinColumn(name = "gatewayUID", nullable = false)
    private Gateway gateway;
}