package com.example.chattigo.model;

import com.example.chattigo.exception.DeviceNotFoundException;
import com.example.chattigo.exception.DevicesExceedsAllowedSizeException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "gateway")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Gateway {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UID", nullable = false)
    private Integer id;

    @Column(name = "serial_number", nullable = false)
    private String serialNumber;

    @Column(name = "human_readable_name", nullable = false)
    private String humanReadableName;

    @Column(name = "ipv4_address", nullable = false)
    private String ipv4Address;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @OneToMany(cascade = {
            CascadeType.ALL}, fetch = FetchType.EAGER, mappedBy = "gateway", orphanRemoval = true)
    private List<PeripheralDevice> peripheralDevices = new LinkedList<>();

    /**
     * Gets peripheral devices.
     *
     * @return the peripheral devices
     */
    public List<PeripheralDevice> getPeripheralDevices() {
        return null != peripheralDevices ? Collections.unmodifiableList(peripheralDevices) : Collections.emptyList();
    }

    /**
     * Sets peripheral devices.
     *
     * @param peripheralDevices the peripheral devices
     */
    public void setPeripheralDevices(List<PeripheralDevice> peripheralDevices) {

        if (peripheralDevices.size() > 10) {
            throw new DevicesExceedsAllowedSizeException();
        }

        this.peripheralDevices = peripheralDevices;
    }

    /**
     * Add  peripheral devices .
     *
     * @param peripheralDevice the peripheral device
     */
    public void addDevice(PeripheralDevice peripheralDevice) {
        peripheralDevices.add(peripheralDevice);
    }

    /**
     * Remove peripheral devices by id.
     *
     * @param id the id device
     */
    public void removeDevice(Integer id) {
        var deleted = peripheralDevices.removeIf(
                peripheralDevice -> peripheralDevice.getId().equals(id));

        if (!deleted) {
            throw new DeviceNotFoundException();
        }
    }
}