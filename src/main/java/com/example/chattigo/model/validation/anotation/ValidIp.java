package com.example.chattigo.model.validation.anotation;


import com.example.chattigo.model.validation.IpConstraintValidator;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = IpConstraintValidator.class)
@Target({FIELD, ANNOTATION_TYPE})
@Documented
@Retention(RUNTIME)
public @interface ValidIp {

    String message() default "ip.invalid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
