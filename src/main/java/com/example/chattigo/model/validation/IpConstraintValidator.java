package com.example.chattigo.model.validation;

import com.example.chattigo.model.validation.anotation.ValidIp;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IpConstraintValidator implements ConstraintValidator<ValidIp, String> {

    @Override
    public void initialize(ValidIp constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {

        var octets = value.split("\\.");

        if(octets.length != 4){
            return false;
        }

        for (String octet : octets) {
            var intValue = Integer.parseInt(octet);
            if (intValue < 0 || intValue > 255) {
                return false;
            }
        }

        return true;
    }
}
