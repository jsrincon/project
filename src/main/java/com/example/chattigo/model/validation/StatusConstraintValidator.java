package com.example.chattigo.model.validation;

import com.example.chattigo.model.enums.StatusDevices;
import com.example.chattigo.model.validation.anotation.ValidStatus;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StatusConstraintValidator implements ConstraintValidator<ValidStatus, CharSequence> {

    private List<String> acceptedValues;

    @Override
    public void initialize(ValidStatus constraintAnnotation) {
        this.acceptedValues = Arrays.stream(constraintAnnotation.anyOf())
                .map(StatusDevices::name)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext constraintValidatorContext) {
        return acceptedValues.contains(value.toString());
    }
}
