package com.example.chattigo.model.dto;

import com.example.chattigo.model.Gateway;
import com.example.chattigo.model.validation.anotation.ValidIp;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GatewayDto implements Serializable {

    @NotBlank
    private String serialNumber;
    @NotBlank
    private String humanReadableName;
    @ValidIp
    private String ipv4Address;
    @Valid
    private List<PeripheralDeviceDto> peripheralDevices;

    public static Gateway dtoToEntity(GatewayDto gatewayDto) {

        return Gateway.builder().serialNumber(gatewayDto.getSerialNumber().trim())
                .humanReadableName(gatewayDto.getHumanReadableName())
                .ipv4Address(gatewayDto.getIpv4Address())
                .build();
    }

    public static GatewayDto entityToDto(Gateway gateway) {

        return GatewayDto.builder().serialNumber(gateway.getSerialNumber())
                .humanReadableName(gateway.getHumanReadableName())
                .ipv4Address(gateway.getIpv4Address()).peripheralDevices(
                        gateway.getPeripheralDevices().stream()
                                .map(PeripheralDeviceDto::entityToDto)
                                .collect(Collectors.toList()))
                .build();
    }
}
