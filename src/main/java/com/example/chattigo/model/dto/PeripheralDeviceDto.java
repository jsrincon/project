package com.example.chattigo.model.dto;

import com.example.chattigo.model.Gateway;
import com.example.chattigo.model.PeripheralDevice;
import com.example.chattigo.model.enums.StatusDevices;
import com.example.chattigo.model.validation.anotation.ValidStatus;
import java.io.Serializable;
import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PeripheralDeviceDto implements Serializable {

    private Integer uid;
    @NotBlank
    private String vendor;
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateCreated;

    @NotNull
    @ValidStatus(anyOf = {StatusDevices.OFFLINE, StatusDevices.ONLINE})
    private String status;

    /**
     * Dto to entity peripheral device.
     *
     * @param peripheralDeviceDto the peripheral device dto
     * @param gateway             the gateway
     * @return the peripheral device
     */
    public static PeripheralDevice dtoToEntity(PeripheralDeviceDto peripheralDeviceDto,
            Gateway gateway) {

        return PeripheralDevice.builder()
                .vendor(peripheralDeviceDto.getVendor())
                .dateCreated(peripheralDeviceDto.getDateCreated())
                .status(StatusDevices.valueOf(peripheralDeviceDto.getStatus()))
                .gateway(gateway)
                .build();
    }

    /**
     * Entity to dto peripheral device dto.
     *
     * @param peripheralDevice the peripheral device
     * @return the peripheral device dto
     */
    public static PeripheralDeviceDto entityToDto(PeripheralDevice peripheralDevice) {
        return PeripheralDeviceDto.builder()
                .uid(peripheralDevice.getId())
                .vendor(peripheralDevice.getVendor())
                .dateCreated(peripheralDevice.getDateCreated())
                .status(peripheralDevice.getStatus().name())
                .build();
    }

}
