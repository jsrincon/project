package com.example.chattigo.model.enums;

public enum StatusDevices {
    ONLINE, OFFLINE
}
