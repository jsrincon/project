package com.example.chattigo.model.internal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ErrorMessage {

    private String code;

    private String message;

    private String type;

    @JsonInclude(Include.NON_EMPTY)
    private List<String> parameters;
}
