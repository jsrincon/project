package com.example.chattigo.exception;

import com.example.chattigo.constants.CodeError;
import com.example.chattigo.constants.MessageError;
import com.example.chattigo.exception.base.ChattigoApiException;
import com.example.chattigo.model.internal.ErrorMessage;
import java.lang.module.ResolutionException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@Slf4j
@ControllerAdvice
public class CustomRestExceptionHandler extends ResolutionException {

    public static final String MALFORMED_JSON = "Malformed JSON";
    public static final String VALIDATION_ERRORS = "Validation errors";
    public static final String BUSINESS_VALIDATION = "Business validation";

    @ExceptionHandler({
            DevicesExceedsAllowedSizeException.class,
            GatewayNotFoundException.class,
            DeviceNotFoundException.class,
            NoAssociatedDevicesException.class,
            SerialNumberAlreadyExistsGatewayException.class
    })
    protected ResponseEntity<Object> handleApiExceptions(ChattigoApiException ex,  WebRequest request) {

        log.error(ex.getMessage());
        ErrorMessage.ErrorMessageBuilder errMessageBuilder = ErrorMessage.builder()
                .code(ex.getErrorCode())
                .message(ex.getMessage())
                .type(BUSINESS_VALIDATION);

        return ResponseEntity
                .status(ex.getStatus())
                .body(errMessageBuilder.build());
    }

    @ExceptionHandler({
            MethodArgumentNotValidException.class
    })
    protected ResponseEntity<Object> handleApiExceptionsValidate(MethodArgumentNotValidException ex, WebRequest request) {

        log.error(ex.getMessage());
        ErrorMessage.ErrorMessageBuilder errMessageBuilder = ErrorMessage.builder()
                .code(CodeError.INVALID_PARAMETERS_ERROR_CODE)
                .message(MessageError.INVALID_VALUES)
                .type(VALIDATION_ERRORS);

        var errors = new ArrayList<String>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        errMessageBuilder.parameters(errors);

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errMessageBuilder.build());
    }
}
