package com.example.chattigo.exception;

import com.example.chattigo.constants.CodeError;
import com.example.chattigo.constants.MessageError;
import com.example.chattigo.exception.base.ChattigoApiException;
import org.springframework.http.HttpStatus;

public class NoAssociatedDevicesException extends ChattigoApiException {

    public NoAssociatedDevicesException() {
        super(MessageError.NO_ASSOCIATED_DEVICE);
    }

    @Override
    public String getErrorCode() {
        return CodeError.ASSOCIATED_DEVICE_ERROR_CODE;
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
