package com.example.chattigo.exception;

import com.example.chattigo.constants.CodeError;
import com.example.chattigo.constants.MessageError;
import com.example.chattigo.exception.base.ChattigoApiException;
import org.springframework.http.HttpStatus;

public class DeviceNotFoundException extends ChattigoApiException {

    public DeviceNotFoundException() {
        super(MessageError.NO_FOUND_DEVICE);
    }

    @Override
    public String getErrorCode() {
        return CodeError.NOT_FOUND_DEVICE_ERROR_CODE;
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
