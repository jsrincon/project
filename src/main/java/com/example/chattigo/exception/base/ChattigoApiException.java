package com.example.chattigo.exception.base;

import org.springframework.http.HttpStatus;

public abstract class ChattigoApiException extends RuntimeException {

    protected ChattigoApiException(String message) {
        super(message);
    }

    public abstract String getErrorCode();

    public abstract HttpStatus getStatus();
}
