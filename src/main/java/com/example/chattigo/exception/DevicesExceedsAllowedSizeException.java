package com.example.chattigo.exception;

import com.example.chattigo.constants.CodeError;
import com.example.chattigo.constants.MessageError;
import com.example.chattigo.exception.base.ChattigoApiException;
import org.springframework.http.HttpStatus;


public class DevicesExceedsAllowedSizeException extends ChattigoApiException {

    public DevicesExceedsAllowedSizeException() {
        super(MessageError.EXCEEDS_SIZE_DEVICE);
    }

    @Override
    public String getErrorCode() {
        return CodeError.INVALID_PARAMETERS_ERROR_CODE;
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
