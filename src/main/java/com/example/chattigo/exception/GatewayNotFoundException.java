package com.example.chattigo.exception;

import com.example.chattigo.constants.CodeError;
import com.example.chattigo.constants.MessageError;
import com.example.chattigo.exception.base.ChattigoApiException;
import org.springframework.http.HttpStatus;

public class GatewayNotFoundException extends ChattigoApiException {

    public GatewayNotFoundException() {
        super(MessageError.NOT_FOUND_GATEWAY);
    }

    @Override
    public String getErrorCode() {
        return CodeError.GATEWAY_NOT_FOUND_ERROR_CODE;
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.NOT_FOUND;
    }
}
