package com.example.chattigo.exception;

import com.example.chattigo.constants.CodeError;
import com.example.chattigo.constants.MessageError;
import com.example.chattigo.exception.base.ChattigoApiException;
import org.springframework.http.HttpStatus;

public class SerialNumberAlreadyExistsGatewayException extends ChattigoApiException {

    public SerialNumberAlreadyExistsGatewayException() {
        super(MessageError.SERIAL_NUMBER_ALREADY_GATEWAY);
    }

    @Override
    public String getErrorCode() {
        return CodeError.SERIAL_ALREADY_EXIST_GATEWAY_ERROR_CODE;
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
