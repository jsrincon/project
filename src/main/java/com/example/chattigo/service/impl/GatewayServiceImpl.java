package com.example.chattigo.service.impl;

import static com.example.chattigo.model.dto.GatewayDto.dtoToEntity;
import static com.example.chattigo.model.dto.GatewayDto.entityToDto;

import com.example.chattigo.exception.DevicesExceedsAllowedSizeException;
import com.example.chattigo.exception.GatewayNotFoundException;
import com.example.chattigo.exception.NoAssociatedDevicesException;
import com.example.chattigo.exception.SerialNumberAlreadyExistsGatewayException;
import com.example.chattigo.model.Gateway;
import com.example.chattigo.model.PeripheralDevice;
import com.example.chattigo.model.dto.GatewayDto;
import com.example.chattigo.model.dto.PeripheralDeviceDto;
import com.example.chattigo.repository.GatewayRepository;
import com.example.chattigo.repository.specification.GatewaySerialNumberSpecification;
import com.example.chattigo.service.IGatewayService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GatewayServiceImpl implements IGatewayService {

    private final GatewayRepository gatewayRepository;

    @Autowired
    public GatewayServiceImpl(GatewayRepository gatewayRepository) {
        this.gatewayRepository = gatewayRepository;
    }

    @Override
    public GatewayDto create(GatewayDto gatewayDto) {

        validateSerialNumber(gatewayDto.getSerialNumber());
        var gateway = dtoToEntity(gatewayDto);
        addDevices(gatewayDto.getPeripheralDevices(), gateway);

        return entityToDto(gatewayRepository.save(gateway));
    }

    @Override
    public GatewayDto addDevice(PeripheralDeviceDto peripheralDevice, String serialNumber) {

        Gateway gateway = getGatewayBySerialNumber(serialNumber);

        validateSizePeripheralDeviceOnGateway(gateway.getPeripheralDevices());
        gateway.addDevice(PeripheralDeviceDto.dtoToEntity(peripheralDevice, gateway));

        return entityToDto(gatewayRepository.save(gateway));
    }

    private Gateway getGatewayBySerialNumber(String serialNumber) {
        return gatewayRepository.findOne(new GatewaySerialNumberSpecification(serialNumber))
                .orElseThrow(GatewayNotFoundException::new);
    }

    @Override
    public GatewayDto removeDevice(String serialNumber, Integer idPeripheralDevice) {

        Gateway gateway = getGatewayBySerialNumber(serialNumber);

        validateEmptyPeripheralDeviceOnGateway(gateway.getPeripheralDevices());
        gateway.removeDevice(idPeripheralDevice);

        return entityToDto(gatewayRepository.save(gateway));
    }

    @Override
    public List<GatewayDto> getAll() {

        return gatewayRepository.findAll().stream().map(GatewayDto::entityToDto).collect(Collectors.toList());
    }

    @Override
    public GatewayDto getBySerialNumber(String serialNumber) {

        return gatewayRepository.findOne(new GatewaySerialNumberSpecification(serialNumber))
                .map(GatewayDto::entityToDto)
                .orElseThrow(GatewayNotFoundException::new);
    }

    /**
     * this method is responsible for mapping the list with the devices associated with the gateway
     *
     * @param peripheralDevices the peripheral device list
     * @param gateway           the gateway to associate
     */
    private void addDevices(List<PeripheralDeviceDto> peripheralDevices, Gateway gateway) {

        if (null != peripheralDevices && !peripheralDevices.isEmpty()) {
            var devices = peripheralDevices
                    .stream()
                    .map(peripheralDevice -> PeripheralDeviceDto.dtoToEntity(peripheralDevice,
                            gateway))
                    .collect(Collectors.toList());

            gateway.setPeripheralDevices(devices);
        }
    }


    /**
     * validate that the device size is greater than 10, if it is, it throws an exception
     *
     * @param peripheralDevice the peripheral device list
     */
    private void validateSizePeripheralDeviceOnGateway(List<PeripheralDevice> peripheralDevice) {

        if (peripheralDevice.size() >= 10) {
            throw new DevicesExceedsAllowedSizeException();
        }
    }

    /**
     * validate that the device size is greater equal 0, if it is, it throws an exception
     *
     * @param peripheralDevice the peripheral device list
     */
    private void validateEmptyPeripheralDeviceOnGateway(List<PeripheralDevice> peripheralDevice) {

        if (peripheralDevice.isEmpty()) {
            throw new NoAssociatedDevicesException();
        }
    }

    /**
     * validate that the serial number is not associated with an existing gateway
     *
     * @param serialNumber input serial number of gateway
     */
    private void validateSerialNumber(String serialNumber) {

        var existGateway = gatewayRepository.findOne(new GatewaySerialNumberSpecification(serialNumber)).isPresent();

        if (existGateway) {
            throw new SerialNumberAlreadyExistsGatewayException();
        }
    }
}
