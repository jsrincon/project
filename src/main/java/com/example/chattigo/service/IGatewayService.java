package com.example.chattigo.service;

import com.example.chattigo.model.dto.GatewayDto;
import com.example.chattigo.model.dto.PeripheralDeviceDto;
import java.util.List;


/**
 * The interface Gateway service.
 */
public interface IGatewayService {

    /**
     * Create gateway.
     *
     * @param gatewayDto the gateway dto
     * @return {@link GatewayDto} the gateway created
     */
    GatewayDto create(GatewayDto gatewayDto);

    /**
     * Add device gateway.
     *
     * @param peripheralDevice the peripheral device
     * @param serialNumber     the serial number of gateway
     * @return {@link GatewayDto} the gateway dto
     */
    GatewayDto addDevice(PeripheralDeviceDto peripheralDevice, String serialNumber);

    /**
     * Remove device gateway.
     *
     * @param serialNumber       the serial number of gateway
     * @param idPeripheralDevice the id peripheral device
     * @return {@link GatewayDto} the gateway dto
     */
    GatewayDto removeDevice(String serialNumber, Integer idPeripheralDevice);

    /**
     * Gets all.
     *
     * @return the all
     */
    List<GatewayDto> getAll();


    /**
     * Gets by serial number.
     *
     * @param serialNumber the serial number
     * @return the by serial number
     */
    GatewayDto getBySerialNumber(String serialNumber);
}
