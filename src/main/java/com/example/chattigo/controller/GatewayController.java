package com.example.chattigo.controller;

import com.example.chattigo.constants.MessageSwagger;
import com.example.chattigo.model.dto.GatewayDto;
import com.example.chattigo.model.dto.PeripheralDeviceDto;
import com.example.chattigo.service.IGatewayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v0/gateways")
@Api(value = "Gateways", tags = {"Gateways"})
public class GatewayController {

    private final IGatewayService gatewayService;

    @Autowired
    public GatewayController(IGatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @PostMapping
    @ApiOperation(value = "create gateway", response = GatewayDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<GatewayDto> create(@Validated @RequestBody GatewayDto gateway) {

        return ResponseEntity.status(HttpStatus.CREATED).body(gatewayService.create(gateway));
    }

    @GetMapping
    @ApiOperation(value = "get all gateway", response = GatewayDto.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<List<GatewayDto>> getAll() {

        return ResponseEntity.status(HttpStatus.OK).body(gatewayService.getAll());
    }

    @GetMapping("/{serial-number}")
    @ApiOperation(value = "get one gateway by serial number", response = GatewayDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<GatewayDto> getBySerialNumber(@PathVariable("serial-number") String serialNumber) {

        return ResponseEntity.status(HttpStatus.OK).body(gatewayService.getBySerialNumber(serialNumber));
    }


    @PatchMapping("/{serial-number}/devices")
    @ApiOperation(value = "add device on gateway", response = GatewayDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 404, message = MessageSwagger.NOT_FOUND),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<GatewayDto> addDevice(
            @Validated @RequestBody PeripheralDeviceDto peripheralDeviceDto,
            @PathVariable(value = "serial-number") String serialNumber) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(gatewayService.addDevice(peripheralDeviceDto, serialNumber));
    }


    @DeleteMapping("/{serial-number}/devices/{uid-device}")
    @ApiOperation(value = "add device on gateway", response = GatewayDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = MessageSwagger.SOMETHING_WRONG),
            @ApiResponse(code = 404, message = MessageSwagger.NOT_FOUND),
            @ApiResponse(code = 500, message = MessageSwagger.INTERNAL_ERROR)})
    public ResponseEntity<GatewayDto> removeDevice(
            @PathVariable(value = "uid-device") Integer uidDevice,
            @PathVariable(value = "serial-number") String serialNumber) {

        return ResponseEntity.status(HttpStatus.OK).body(gatewayService.removeDevice(serialNumber,uidDevice));
    }
}
