package com.example.chattigo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChattigoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChattigoApplication.class, args);
	}

}
