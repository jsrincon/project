package com.example.chattigo.repository.specification;

import com.example.chattigo.model.Gateway;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class GatewaySerialNumberSpecification implements Specification<Gateway> {

    private final String parameter;

    public GatewaySerialNumberSpecification(String parameter) {
        this.parameter = parameter;

    }

    @Override
    public Predicate toPredicate(Root<Gateway> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        return criteriaBuilder.equal(criteriaBuilder.upper(root.get("serialNumber")), parameter.toUpperCase().trim());
    }
}
