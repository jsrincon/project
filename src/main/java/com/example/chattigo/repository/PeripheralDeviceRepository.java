package com.example.chattigo.repository;

import com.example.chattigo.model.PeripheralDevice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeripheralDeviceRepository extends JpaRepository<PeripheralDevice, Integer> {

}