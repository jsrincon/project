package com.example.chattigo.repository;

import com.example.chattigo.model.Gateway;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface GatewayRepository extends JpaRepository<Gateway, Integer>,
        JpaSpecificationExecutor<Gateway> {
}