CREATE TABLE gateway
(
    UID                 int(10)      NOT NULL AUTO_INCREMENT,
    serial_number       varchar(255) NOT NULL UNIQUE,
    human_readable_name varchar(255) NOT NULL,
    ipv4_address        varchar(255) NOT NULL,
    PRIMARY KEY (UID)
);
CREATE TABLE peripheral_device
(
    UID          int(10)      NOT NULL AUTO_INCREMENT,
    vendor       varchar(255) NOT NULL,
    date_created date         NOT NULL,
    status       varchar(255) NOT NULL,
    gatewayUID   int(10)      NOT NULL,
    PRIMARY KEY (UID)
);
ALTER TABLE peripheral_device
    ADD CONSTRAINT fk_device_gateway FOREIGN KEY (gatewayUID) REFERENCES gateway (UID);
