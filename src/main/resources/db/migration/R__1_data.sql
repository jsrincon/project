INSERT INTO gateway (serial_number, human_readable_name, ipv4_address)
VALUES ('5012345678900', 'Gateway exito', '192.168.1.10'),
       ('5012345678700', 'Gateway alkosto', '192.168.1.11'),
       ('5012345678600', 'Gateway ktronic', '192.168.1.12'),
       ('5012345678500', 'Gateway falabella', '192.168.1.13'),
       ('5012345678400', 'Gateway constructor', '192.168.1.14'),
       ('5012345678300', 'Gateway pasarella', '192.168.1.15'),
       ('5012345678200', 'Gateway c.c unico', '192.168.1.16'),
       ('5012345678100', 'Gateway cañaveral', '192.168.1.17'),
       ('5012345678000', 'Gateway c.c cosmocentro', '192.168.1.18');


INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'SAMSUNG X1', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678900';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'SAMSUNG X2', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678900';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'SAMSUNG X3', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678900';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'SAMSUNG X4', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678900';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'SAMSUNG X5', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678900';


INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'MOTOROLA X1', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678600';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'MOTOROLA X2', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678600';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'MOTOROLA X3', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678600';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'MOTOROLA X4', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678600';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'MOTOROLA X5', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678600';

INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'HUAWEI X1', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678300';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'HUAWEI X2', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678300';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'HUAWEI X3', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678300';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'HUAWEI X4', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678300';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'HUAWEI X5', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678300';


INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'LG X1', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678000';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'LG X2', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678000';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'LG X3', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678000';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'LG X4', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678000';
INSERT INTO peripheral_device
    (vendor, date_created, status, gatewayUID)
select 'LG X5', '2020-09-01', 'ONLINE', g.UID
from gateway g
where serial_number = '5012345678000';